from django.contrib import admin
from .models import Post, APost, AReps

# Register your models here.
admin.site.register(Post)
admin.site.register(APost)
admin.site.register(AReps)

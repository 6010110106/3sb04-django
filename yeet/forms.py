from django import forms
from .models import Post, APost, AReps

class PostForm(forms.ModelForm):
    class Meta:
        model = APost
        fields = ('author', 'title', 'text')
        labels = {
            'author': 'Author (optional)',
            'title': 'Title (optional)'
        }

class ReplyForm(forms.ModelForm):
    class Meta:
        model = AReps
        fields = ('author', 'text')
        labels = {
            'author': 'Author (optional)',
        }

from django.conf import settings
from django.db import models
from django.utils import timezone

# Create your models here.
class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class APost(models.Model):
    title = models.CharField(max_length=64,blank=True)
    author = models.CharField(max_length=64,blank=True)
    text = models.TextField()
    image = models.ImageField(upload_to='uploads/',blank=True,null=True)
    post_date = models.DateTimeField(default=timezone.now)

    def publish(self):
        self.post_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class AReps(models.Model):
    post = models.ForeignKey(
        'yeet.APost',
        on_delete=models.CASCADE, 
        related_name='replies'
    )
    author = models.CharField(max_length=64,blank=True)
    text = models.TextField()
    image = models.ImageField(upload_to='uploads/',blank=True,null=True)
    post_date = models.DateTimeField(default=timezone.now)



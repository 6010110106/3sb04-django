from django.urls import path
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/<int:pk>/reply', views.add_reply, name="add_reply"),
    path('post/new', views.post_new, name='post_new'),
    path('yeet', views.index, name='yeet')
]

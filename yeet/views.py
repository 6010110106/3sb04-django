from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.http import HttpResponse

from .models import Post, APost, AReps
from .forms import PostForm, ReplyForm

# Create your views here.

def post_list(request):
    posts = APost.objects.filter(
        post_date__lte=timezone.now()
    ).order_by('-post_date')
    return render(request, 'yeet/post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(APost, pk=pk)
    return render(request, 'yeet/post_details.html', {'post': post})

def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST,initial={'author':'Anonymous'})
        if form.is_valid():
            post = form.save(commit=False)
            if not post.author:
                post.author = 'Anonymous'
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'yeet/post_new.html', {'form':form})

def add_reply(request, pk):
    post = get_object_or_404(APost, pk=pk)
    if request.method == 'POST':
        form = ReplyForm(request.POST,initial={'author':'Anonymous'})
        if form.is_valid():
            reply = form.save(commit=False)
            reply.post = post
            if not reply.author:
                post.author = 'Anonymous'
            reply.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = ReplyForm()
    return render(request, 'yeet/add_reply.html', {'form':form}) 

def index(request):
    return HttpResponse("YEET")
